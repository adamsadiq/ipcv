/////////////////////////////////////////////////////////////////////////////
//
// COMS30121 - draw.cpp
// TOPIC: create, draw and save an image
//
// Getting-Started-File for OpenCV
// University of Bristol
//
/////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <opencv/cv.h>        //you may need to
#include <opencv/highgui.h>   //adjust import locations
#include <opencv/cxcore.h>    //depending on your machine setup
#include <math.h>             // sqrt


using namespace cv;


/*

Equivalences of OPENCV Mat Types and C types

OPENCV
CV_8U - 8-bit unsigned integers ( 0..255 )
CV_8S - 8-bit signed integers ( -128..127 )
CV_16U - 16-bit unsigned integers ( 0..65535 )
CV_16S - 16-bit signed integers ( -32768..32767 )
CV_32S - 32-bit signed integers ( -2147483648..2147483647 )
CV_32F - 32-bit floating-point numbers ( -FLT_MAX..FLT_MAX, INF, NAN )
CV_64F - 64-bit floating-point numbers ( -DBL_MAX..DBL_MAX, INF, NAN )
enum { CV_8U=0, CV_8S=1, CV_16U=2, CV_16S=3, CV_32S=4, CV_32F=5, CV_64F=6 };

C
8-bit unsigned integer (uchar)
8-bit signed integer (schar)
16-bit unsigned integer (ushort)
16-bit signed integer (short)
32-bit signed integer (int)
32-bit floating-point number (float)
64-bit floating-point number (double)

*/


//void sobel(cv::Mat &image, cv::Mat &sobelImageDX, cv::Mat &sobelImageDXOutput, cv::Mat &sobelImageDYOutput, cv::Mat &sobelImageDY, cv::Mat &output); // BEFORE
void sobel(cv::Mat &image, cv::Mat &sobelImageDX, cv::Mat &sobelImageDXOutput, cv::Mat &sobelImageDY, cv::Mat &sobelImageDYOutput, cv::Mat &output); //NEW - Wrong position in sobelImageDYOutput and sobelImageDY when calling function

int main( int argc, char** argv){


  char* imageName = argv[1];

  Mat image;
  image = imread( imageName, 1 );
  imshow("Original", image); // NEW - Just showing the image for easier visualisation

  if( argc != 2 || !image.data )
  {
    printf( " No image data \n " );
    return -1;
  }
  Mat sobelImage;
  Mat sobelImageDX;
  Mat sobelImageDY;
  Mat gray_image;
  Mat output;
  Mat sobelImageDXOutput;
  Mat sobelImageDYOutput;

  // image.convertTo(gray_image_32, CV_32F);

  cvtColor( image, gray_image, CV_BGR2GRAY );
  imshow("Gray", gray_image); // NEW - Just showing the image for easier visualisation
  // imwrite( "sobel-before.jpg", sobelImageDX + sobelImageDY );



  sobel(gray_image, sobelImageDX, sobelImageDXOutput, sobelImageDY, sobelImageDYOutput, output); // SAME AS BEFORE but here you are sending first "sobelImageDY" and after "sobelImageDYOutput". Opposite to the function definition

  imshow("Sobel Normalised X", sobelImageDXOutput); // NEW - Just showing the image for easier visualisation
  imshow("Sobel Normalised Y ", sobelImageDYOutput); // NEW - Just showing the image for easier visualisation
  imshow("Magnitudes", output); // NEW - Just showing the image for easier visualisation


  // COMMENTED AS IMAGES ARE SHOWN IN THE PREVIOUS LINES
  /*
  imwrite("result.jpg", sobelImageDX);

  //convert image from float to uchar
  // Mat output;

  imwrite( "dx.jpg", sobelImageDXOutput );
  // imwrite( "dy.jpg",  sobelImageDY );

  imwrite( "sobel.jpg", sobelImageDX + sobelImageDY );
*/


  waitKey(0); // NEW - Just for pressing a key when showing images
}

//  void sobel(cv::Mat &image, cv::Mat &sobelImageDX, cv::Mat &sobelImageDXOutput, cv::Mat &sobelImageDYOutput, cv::Mat &sobelImageDY, cv::Mat &output){ // BEFORE
  void sobel(cv::Mat &image, cv::Mat &sobelImageDX, cv::Mat &sobelImageDXOutput, cv::Mat &sobelImageDY, cv::Mat &sobelImageDYOutput, cv::Mat &output){ //NEW - Wrong position in sobelImageDYOutput and sobelImageDY when calling function

    // intialise the output using the input image
    // sobelImageDX.create(image.size(), image.type()); // BEFORE (This create an uchar Mat)
    sobelImageDX.create(image.size(), CV_32S); // NEW - Sobel gives as a result negative values. "int" is required
    //sobelImageDY.create(image.size(), image.type()); // BEFORE
    sobelImageDY.create(image.size(), CV_32S); // NEW - Sobel gives as a result negative values. "int" is required

    cv::Mat outputUnnorm;
    outputUnnorm.create(image.size(), CV_32F); // NEW - Sobel magnitudes are float values

    Mat_ <int> dx(3,3);
    //dx << 1,0,1,-2,0,2,-1,0,1; // BEFORE
    dx << -1,0,1,-2,0,2,-1,0,1; // NEW - Wrong value in first element of the kernel

    Mat_ <int> dy(3,3);
    dy << -1,-2,-1,0,0,0,1,2,1;

    // now we can do the convoltion
  	for ( int i = 1; i < image.rows-1; i++ )
  	{
  		for( int j = 1; j < image.cols-1; j++ )
  		{
  			int sumX = 0;
                        int sumY = 0; // NEW - You can compute also for Y
  			for( int m = -1; m <= 1; m++ )
  			{
  				for( int n = -1; n <= 1; n++ )
  				{
  					// find the correct indices we are using

  					//int imagex = i + m ; // BEFORE - (this can also works but it's correlation)
  					int imagex = i - m ; // NEW - Just do convolution and not correlation

  					// int imagey = j + n ; // BEFORE
  					int imagey = j - n ; // NEW - convolution as the slides specify (but correlation also works)

  					int kernelx = m + 1;
  					int kernely = n + 1;

  					// get the values from the padded image and the kernel
  					int imageval = ( int ) image.at<uchar>( imagex, imagey ); //BEFORE

  					//int kernalval = dx.at<int>( kernelx, kernely ); //BEFORE You only compute X
 					int kernalvalX = dx.at<int>( kernelx, kernely ); //NEW - You can for both X and Y derivatives
 					int kernalvalY = dy.at<int>( kernelx, kernely ); // NEW - You can compute also for Y derivative

  					// do the multiplication
  					sumX += imageval * kernalvalX;
  					sumY += imageval * kernalvalY;
            // printf("%d", sumX);
            // printf("\n");
  				}
  			}
  			// set the output value as the sum of the convolution
                        // sobelImageDX.at<uchar>(i, j) = (uchar) sumX;
  			sobelImageDX.at<int>(i, j) =  sumX; //NEW
  			sobelImageDY.at<int>(i, j) =  sumY; // NEW - You can compute also for Y (but up to you)
                        outputUnnorm.at<float>(i, j) =  sqrt( sumX*sumX + sumY*sumY); // and get magnitude in same loop

        // printf("%lf",sobelImageDX.at<double>(i, j));
  		}
  	}


    // imwrite("result", sobelImageDX);

    //cv::normalize(sobelImageDX, sobelImageDXOutput, 0, 255, NORM_MINMAX, CV_32F); // BEFORE
    cv::normalize(sobelImageDX, sobelImageDXOutput, 0, 255, NORM_MINMAX, CV_8U); // NEW - Better to fit in uchar (CV_8U)
    cv::normalize(sobelImageDY, sobelImageDYOutput, 0, 255, NORM_MINMAX, CV_8U); // NEW - Y derivative
    cv::normalize(outputUnnorm, output, 0, 255, NORM_MINMAX, CV_8U); // NEW - and the output

    // for ( int i = 1; i < image.rows-1; i++ )
  	// {
  	// 	for( int j = 1; j < image.cols-1; j++ )
  	// 	{
  	// 		int sumY = 0.0;
  	// 		for( int m = -1; m <= 1; m++ )
  	// 		{
  	// 			for( int n = -1; n <= 1; n++ )
  	// 			{
  	// 				// find the correct indices we are using
  	// 				int imagex = i + m + 1;
  	// 				int imagey = j + n + 1;
  	// 				int kernelx = m + 1;
  	// 				int kernely = n + 1;
    //
  	// 				// get the values from the padded image and the kernel
  	// 				int imageval = ( int ) image.at<uchar>( imagex, imagey );
  	// 				double kernalval = dy.at<double>( kernelx, kernely );
    //
  	// 				// do the multiplication
  	// 				sumY += imageval * kernalval;
  	// 			}
  	// 		}
    //     // printf("%d", sumY);
  	// 		// set the output value as the sum of the convolution
  	// 		sobelImageDY.at<double>(i, j) = (uchar) sumY;
  	// 	}
  	// }
    //
    //   // Mat output;
    //     for ( int i = 0; i < image.rows; i++ ){
    //   		for( int j = 0; j < image.cols; j++ ){
    //         // printf("%u", sobelImageDY.at<uchar>(1, 1));
    //         // output.at<float>(i,j) = (uchar) hypot(sobelImageDX.at<uchar>(i, j),sobelImageDY.at<uchar>(i, j));
    //       }
    //     }

}
